﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FileStorage.DAL.Abstract;
using FileStorage.DAL.Repositories;

namespace FileStorage.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly FileStorageDbContext fileStorageDbContext;
        private IFileInfoRepository fileInfoRepository;
        private IFileStatusRepository fileStatusRepository;
        private IShortLinkRepository shortLinkRepository;
       
        public UnitOfWork(FileStorageDbContext fileStorageDbContext) => this.fileStorageDbContext = fileStorageDbContext;

        public IFileInfoRepository FileInfoRepository => fileInfoRepository ??=new FileInfoRepository(fileStorageDbContext);
        public IFileStatusRepository FileStatusRepository => fileStatusRepository ??= new FileStatusRepository(fileStorageDbContext);
        public IShortLinkRepository ShortLinkRepository => shortLinkRepository ??= new ShortLinkRepository(fileStorageDbContext);
       

        public async Task<int> SaveAsync() => await fileStorageDbContext.SaveChangesAsync();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileStorage.DAL.Abstract;
using FileStorage.Domain;
using Microsoft.EntityFrameworkCore;

namespace FileStorage.DAL.Repositories
{
    public class FileInfoRepository : Repository<FileInfo>, IFileInfoRepository
    {
        public FileInfoRepository(FileStorageDbContext fileStorageDbContext) : base(fileStorageDbContext)
        {
        }
        public async Task<FileInfo> GetByIdWithDetailsAsync(int id)
        {
            var fileInfo = await FileStorageDbContext.FileInfos.FindAsync(id);
            await FileStorageDbContext.Entry(fileInfo).Reference(f => f.ShortLink).LoadAsync();
            await FileStorageDbContext.Entry(fileInfo).Reference(f => f.FileStatus).LoadAsync();
            return fileInfo;
        }
        public IQueryable<FileInfo> GetAllWithDetails()
        {
            return FileStorageDbContext.FileInfos
                .Include(f => f.ShortLink)
                .Include(f => f.FileStatus)
                .AsQueryable();
        }


        public void UpdateNameAndStatus(FileInfo fileInfo)
        {
            FileStorageDbContext.FileInfos.Attach(fileInfo);
            FileStorageDbContext.Entry(fileInfo)
                .Property(x => x.FileStatus).IsModified = true;
            FileStorageDbContext.Entry(fileInfo)
                .Property(x => x.Name).IsModified = true;
        }


    }
}

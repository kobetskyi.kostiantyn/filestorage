﻿using System;
using System.Collections.Generic;
using System.Text;
using FileStorage.DAL.Abstract;
using FileStorage.Domain;

namespace FileStorage.DAL.Repositories
{
    public class FileStatusRepository: Repository<FileStatus>, IFileStatusRepository
    {
        public FileStatusRepository(FileStorageDbContext fileStorageDbContext) : base(fileStorageDbContext)
        {
        }
    }
}

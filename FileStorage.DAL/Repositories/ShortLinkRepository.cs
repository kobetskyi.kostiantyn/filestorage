﻿using FileStorage.DAL.Abstract;
using FileStorage.Domain;
using System.Linq;

namespace FileStorage.DAL.Repositories
{
    public class ShortLinkRepository : Repository<ShortLink>, IShortLinkRepository
    {
        public ShortLinkRepository(FileStorageDbContext fileStorageDbContext) : base(fileStorageDbContext)
        {
        }
    }
}

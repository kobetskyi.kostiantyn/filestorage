﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FileStorage.Domain;
using FileStorage.Domain.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using FileInfo = FileStorage.Domain.FileInfo;

namespace FileStorage.DAL.Services
{
    public class FileStorageDbInitializer
    {
        private readonly FileStorageDbContext context;
        private readonly ILogger<FileStorageDbInitializer> logger;
        private readonly RoleManager<Role> roleManager;
        private readonly UserManager<User> userManager;

        public FileStorageDbInitializer(
            FileStorageDbContext context,
            ILogger<FileStorageDbInitializer> logger,
            RoleManager<Role> roleManager,
            UserManager<User> userManager)
        {
            this.context = context;
            this.logger = logger;
            this.roleManager = roleManager;
            this.userManager = userManager;
        }

        public void Initialize()
        {
            logger.LogInformation("Db initialization...");
            var db = context.Database;

            #region Delete-Create
            //db.EnsureDeleted();
            //logger.LogInformation("Db deleted");

            //db.EnsureCreated();
            //logger.LogInformation("Db created");
            #endregion

            #region Migrations
            //if (db.GetPendingMigrations().Any())
            //{
            //    db.Migrate();
            //} 
            #endregion

            #region Statuses
            try
            {
                logger.LogInformation("FileInfo statuses initialization...");
                InitializeFileStatuses();
            }
            catch (Exception e)
            {
                logger.LogError(e, "FileInfo statuses Initialization ERROR");
                throw;
            }
            logger.LogInformation("FileInfo statuses were initialized");
            #endregion

            #region Links

            try
            {
                logger.LogInformation("ShortLinks initialization...");
                InitializeShortLinks();
            }
            catch (Exception e)
            {
                logger.LogError(e, "ShortLinks initialization ERROR");
                throw;
            }
            logger.LogInformation("ShortLinks were initialized");

            logger.LogInformation("Db initialized");
            #endregion

            #region Users
            try
            {
                logger.LogInformation("Users initialization...");
                InitializeIdentity().Wait();
            }
            catch (Exception e)
            {
                logger.LogError(e, "Users initialization ERROR");
                throw;
            }
            logger.LogInformation("Users were initialized");

            logger.LogInformation("Db initialized");
            #endregion
        }

        private async Task InitializeIdentity()
        {


            async Task CheckRole(string roleName)
            {
                if (!await roleManager.RoleExistsAsync(roleName))
                {
                    logger.LogInformation("{0} role doesn't exist. Creating role...", roleName);
                    await roleManager.CreateAsync(new Role() { Name = roleName });
                }
            }

            await CheckRole(Constants.AdministratorRoleName);
            await CheckRole(Constants.UserRoleName);


            if (await userManager.FindByEmailAsync("Admin@gmail.com") is null)
            {
                logger.LogInformation("user with email 'Admin@gmail.com' doesn't exist. Begin creating user.");
                var admin = new User()
                {
                    UserName = "Admin@gmail.com",
                    Email = "Admin@gmail.com"
                };
                var creationResult = await userManager.CreateAsync(admin, "Admin@123");
                if (creationResult.Succeeded)
                {
                    var result = await userManager.AddToRoleAsync(admin, Constants.AdministratorRoleName);
                    if (!result.Succeeded)
                        logger.LogError("could not set role for admin user");

                }
                else
                {
                    var errors = creationResult.Errors.Select(e => e.Description);
                    throw new InvalidOperationException($"Error while creating admin user {string.Join(',', errors)}");
                }
            }

            if (await userManager.FindByEmailAsync("User@gmail.com") is null)
            {
                logger.LogInformation("user with email 'User@gmail.com' doesn't exist. Begin creating user.");
                var admin = new User()
                {
                    UserName = "User@gmail.com",
                    Email = "User@gmail.com"
                };
                var creationResult = await userManager.CreateAsync(admin, "User@123");
                if (creationResult.Succeeded)
                {
                    var result = await userManager.AddToRoleAsync(admin, Constants.UserRoleName);
                    if (!result.Succeeded)
                        logger.LogError("could not set role for regular user");
                }
                else
                {
                    var errors = creationResult.Errors.Select(e => e.Description);
                    throw new InvalidOperationException($"Error while creating regular user {string.Join(',', errors)}");
                }
            }


        }

        private void InitializeFileStatuses()
        {
            if (context.FileStatuses.Any())
            {
                logger.LogInformation("FileStatuses table already has data. Initialization not needed");
                return;
            }

            using (context.Database.BeginTransaction())
            {
                context.FileStatuses.AddRange(new[]
                {
                    new FileStatus(){StatusName = "Private"},
                    new FileStatus(){StatusName = "Shared"}
                });
                context.SaveChanges();
                context.Database.CommitTransaction();
            }
        }

        private void InitializeShortLinks()
        {
            using (context.Database.BeginTransaction())
            {
                if (context.ShortLinks.Any())
                {
                    logger.LogInformation("ShortLinks table already has data. Initialization not needed");
                    return;
                }

                context.ShortLinks.AddRange(new[]
                {
                    new ShortLink(){Value = "AAAAAAB"},
                    new ShortLink(){Value = "AAAAAAC"},
                    new ShortLink(){Value = "AAAAAAG"},
                    new ShortLink(){Value = "AAAAAAF"},
                    new ShortLink(){Value = "AAAAAAR"},
                    new ShortLink(){Value = "AAAAAAU"},
                    new ShortLink(){Value = "AAAAAAJ"},
                    new ShortLink(){Value = "AAAAAAM"},
                    new ShortLink(){Value = "AAAAAAP"},
                    new ShortLink(){Value = "AAAAAAD"},
                    new ShortLink(){Value = "AAAAAAN"},
                    new ShortLink(){Value = "AAAAAAH"},
                    new ShortLink(){Value = "AAAAAAQ"},
                    new ShortLink(){Value = "AAAAAAT"},
                    new ShortLink(){Value = "AAAAAAS"},
                    new ShortLink(){Value = "AAAAAAO"},
                    new ShortLink(){Value = "AAAAAAE"},
                    new ShortLink(){Value = "AAAAAAI"},
                    new ShortLink(){Value = "AAAAAAK"},
                    new ShortLink(){Value = "AAAAAAL"}
                });
                context.SaveChanges();

                context.Database.CommitTransaction();
            }
        }
    }
}

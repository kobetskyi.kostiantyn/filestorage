﻿using System;
using System.Collections.Generic;
using System.Text;
using FileStorage.Domain;

namespace FileStorage.DAL.Abstract
{
    public interface IFileStatusRepository: IRepository<FileStatus>
    {
    }
}

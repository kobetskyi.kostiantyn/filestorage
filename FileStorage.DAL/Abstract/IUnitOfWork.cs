﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FileStorage.DAL.Abstract
{
    public interface IUnitOfWork
    {
        IFileInfoRepository FileInfoRepository { get; }
        IFileStatusRepository FileStatusRepository { get; }
        IShortLinkRepository ShortLinkRepository { get; }
        Task<int> SaveAsync();
    }
}

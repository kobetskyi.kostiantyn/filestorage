﻿using System.Linq;
using System.Threading.Tasks;
using FileStorage.Domain.Abstract;
using Microsoft.EntityFrameworkCore;

namespace FileStorage.DAL.Abstract
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        protected FileStorageDbContext FileStorageDbContext { get; set; }

        protected Repository(FileStorageDbContext fileStorageDbContext)
        {
            FileStorageDbContext = fileStorageDbContext;
        }

        public IQueryable<TEntity> FindAll()
        {
            return FileStorageDbContext.Set<TEntity>();
        }

        public async Task<TEntity> GetByIdAsync(int id)
        {
            return await FileStorageDbContext.Set<TEntity>().FindAsync(id);
        }

        public async Task AddAsync(TEntity entity)
        {
            await FileStorageDbContext.Set<TEntity>().AddAsync(entity);
        }

        public void Update(TEntity entity)
        {
            FileStorageDbContext.Set<TEntity>().Attach(entity);
            FileStorageDbContext.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(TEntity entity)
        {
            FileStorageDbContext.Set<TEntity>().Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            Delete(await GetByIdAsync(id));
        }
    }

}

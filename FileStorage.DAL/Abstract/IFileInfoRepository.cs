﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileStorage.Domain;

namespace FileStorage.DAL.Abstract
{
    public interface IFileInfoRepository:IRepository<FileInfo>
    {
        Task<FileInfo> GetByIdWithDetailsAsync(int id);
        IQueryable<FileInfo> GetAllWithDetails();
        void UpdateNameAndStatus(FileInfo fileInfo);
    }
}

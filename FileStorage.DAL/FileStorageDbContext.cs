﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Text;
using FileStorage.Domain;
using FileStorage.Domain.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Microsoft.Extensions.Options;

namespace FileStorage.DAL
{
    public class FileStorageDbContext : IdentityDbContext<User, Role, int> 
    {
        public FileStorageDbContext(DbContextOptions options) : base(options) {}
        public DbSet<FileInfo> FileInfos { get; set; }
        public DbSet<FileStatus> FileStatuses { get; set; }
        public DbSet<ShortLink> ShortLinks { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            var boolConverter = new BoolToZeroOneConverter<byte>();

            #region FileInfo
            builder.Entity<FileInfo>()
                .Property(f => f.Name)
                .HasMaxLength(250);
            builder.Entity<FileInfo>()
                .Property(f => f.Created)
                .HasColumnType("datetime2")
                .HasDefaultValueSql("GETDATE()");
            builder.Entity<FileInfo>()
                .Property(f => f.Changed)
                .HasColumnType("datetime2")
                .HasDefaultValueSql("GETDATE()");
            builder.Entity<FileInfo>()
                .Property(f => f.FileSize)
                .HasColumnType("bigint");
            builder.Entity<FileInfo>()
                .HasOne(f => f.User)
                .WithMany(u => u.FileInfos);
            builder.Entity<FileInfo>()
                .HasOne(f => f.FileStatus)
                .WithMany(u => u.FileInfos)
                .IsRequired();
            #endregion

            #region ShortLink
            builder.Entity<ShortLink>()
                .Property(s => s.Value)
                .HasMaxLength(8);
            builder.Entity<ShortLink>()
                .Property(s => s.ConcurrencyToken)
                .IsRowVersion();
            builder.Entity<ShortLink>()
                .Property(s => s.Taken)
                .HasConversion(boolConverter);
            #endregion

            #region FileStatus
            builder.Entity<FileStatus>()
                .Property(f => f.StatusName)
                .HasMaxLength(50);
            #endregion

            #region User
            builder.Entity<User>()
                .HasKey(u => u.Id);
            #endregion


        }
    }
}

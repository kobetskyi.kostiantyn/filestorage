﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using FileStorage.BLL.Models;
using FileStorage.BLL.Services.Abstract;
using FileStorage.Domain.Identity;
using FileStorage.PL.Controllers;
using FileStorage.PL.Models;
using FileStorage.PL.Services.Abstract;
using FileStorage.PL.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace FileStorage.PL.Tests.Controllers
{
    [TestFixture()]
    public class UserFilesControllerTests
    {
        private Mock<IFileInfoService> filesService;
        private Mock<UserManager<User>> userManager;
        private Mock<IHostingFileService> hostingFileService;
        private PagedFileModelList pagedFileModelList;
        private FileInfoModel[] fileInfoModels;
        private IEnumerable<StatusModel> stauses;
        
        private List<User> users;
        private ClaimsPrincipal user;

        [SetUp]
        public void Setup()
        {

            users = new List<User>()
            {
                new User() { UserName = "User@gmail.com", Email = "User@gmail.com", Id = 2 },
                new User() { UserName = "User2@gmail.com", Email = "User2@gmail.com", Id = 3 },
            };
            filesService = new Mock<IFileInfoService>();
            userManager = MockUserManager(users);
            hostingFileService = new Mock<IHostingFileService>();
            fileInfoModels = new FileInfoModel[]
            {
                new FileInfoModel()
                {
                    Changed = new DateTime(2021,09,19),
                    FileSize = 825701,
                    Id=1,
                    StatusId = 2,
                    Link = "AAAAAAA",
                    Name = "File-1",
                    Path = "Path-1"
                },
                new FileInfoModel()
                {
                    Changed = new DateTime(2021,09,18),
                    FileSize = 839655,
                    Id=2,
                    StatusId = 2,
                    Link = "AAAAAAB",
                    Name = "File-2",
                    Path = "Path-2"
                },
                new FileInfoModel()
                {
                    Changed = new DateTime(2021,09,17),
                    FileSize = 813768,
                    Id=3,
                    StatusId = 2,
                    Link = "AAAAAAC",
                    Name = "File-3",
                    Path = "Path-3"
                },
                new FileInfoModel()
                {
                    Changed = new DateTime(2021,09,16),
                    FileSize = 813768,
                    Id=4,
                    StatusId = 2,
                    Link = "AAAAAAD",
                    Name = "File-4",
                    Path = "Path-4"
                },
            };
            stauses = new List<StatusModel>()
            {
                new StatusModel(){Id=1, Value = "Private"},
                new StatusModel(){Id=2, Value = "Shared"}
            };
            pagedFileModelList = new PagedFileModelList(fileInfoModels.ToList(), 4, 1, 5);

            user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[] {
                new Claim(ClaimTypes.NameIdentifier, "user"),
                new Claim(ClaimTypes.Name, "UserName"),
                new Claim(ClaimTypes.Email, "UserEmail")
            }, "TestAuthentication"));
        }

        [Test()]
        public async Task GetAll_ReturnsExpectedResult()
        {
            filesService.Setup(service =>  service.GetAllUserFiles(It.IsAny<User>(), It.IsAny<FileInfoFilterModel>()))
                .ReturnsAsync(pagedFileModelList);

            filesService.Setup(service => service.GetStatuses()).Returns(stauses);

            userManager.Setup(u => u.FindByEmailAsync(It.IsAny<string>())).ReturnsAsync(users[0]);
            
            var controller = new UserFilesController(filesService.Object, userManager.Object,
                It.IsAny<IHostingFileService>());

            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext(){User = user};

            var result = await controller.GetAll(It.IsAny<FileInfoFilterModel>());
            var okResult = result as OkObjectResult;
            
            Assert.IsNotNull(okResult);
            Assert.IsInstanceOf<FilesListResponse>(okResult.Value);
        }


        [Test()]
        [TestCase("")]
        [TestCase("aaaaaaab")]
        [TestCase("aaaaac")]
        public void GetById_WithBadLink_ThrowsException(string link)
        {
            var controller = new UserFilesController(It.IsAny<IFileInfoService>(), userManager.Object,
                It.IsAny<IHostingFileService>());

            Assert.Throws<PlBadRequestException>(() => controller.GetByLink(link));
        }

        [Test]
        [TestCase("aaaaaaa")]
        [TestCase("aaaaaab")]
        [TestCase("aaaaaac")]
        public void GetByLink_WithCorrectLink_ReturnsExpectedResult(string link)
        {

            filesService.Setup(service => service.GetByLink(link))
                .Returns(fileInfoModels[0]);
                
            var controller = new UserFilesController(filesService.Object, userManager.Object,
                It.IsAny<IHostingFileService>());
            
            var result = controller.GetByLink(link);
            var okResult = result as OkObjectResult;

            Assert.IsNotNull(okResult);
            Assert.IsInstanceOf<FileViewModel>(okResult.Value);
        }


        private static Mock<UserManager<TUser>> MockUserManager<TUser>(List<TUser> ls) where TUser : class
        {
            var store = new Mock<IUserStore<TUser>>();
            var mgr = new Mock<UserManager<TUser>>(store.Object, null, null, null, null, null, null, null, null);
            mgr.Object.UserValidators.Add(new UserValidator<TUser>());
            mgr.Object.PasswordValidators.Add(new PasswordValidator<TUser>());

            mgr.Setup(x => x.DeleteAsync(It.IsAny<TUser>())).ReturnsAsync(IdentityResult.Success);
            mgr.Setup(x => x.CreateAsync(It.IsAny<TUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success).Callback<TUser, string>((x, y) => ls.Add(x));
            mgr.Setup(x => x.UpdateAsync(It.IsAny<TUser>())).ReturnsAsync(IdentityResult.Success);

            return mgr;
        }
    }
}
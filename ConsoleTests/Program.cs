﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;

namespace ConsoleTests
{

    public static class ListExtension
    {
        public static void Shuffle<T>(this List<T> list, Random rnd)
        {
            var n = list.Count;
            while (n > 1)
            {
                var k = rnd.Next(n--);
                (list[n], list[k]) = (list[k], list[n]);
            }
        }
    }

    class Program
    {

        private static string GetFileName(string fileName, DirectoryInfo dir)
        {
            var dirFiles = dir.GetFiles();
            var fileNameHasDuplicates = true;
            var i = 0;
            var prefix = " (copy)";
            while (fileNameHasDuplicates)
            {
                var splittedName = fileName.Split('.', StringSplitOptions.RemoveEmptyEntries);
                var extension = splittedName.LastOrDefault() is null ? "" : splittedName.Last();
                var nameWitoutExtension = extension.Length > 0 ? fileName[..^(extension.Length + 1)] : fileName;

                if (dirFiles.Any(f => f.Name == fileName))
                {
                    if (i < 1)
                        nameWitoutExtension += prefix;
                    else
                    {
                        var prefixLength = prefix.Length;
                        prefix = $" (copy {i})";
                        nameWitoutExtension = nameWitoutExtension[..^prefixLength] + prefix;
                    }

                    i++;
                    fileName = nameWitoutExtension + "." + extension;
                }
                else
                {
                    fileNameHasDuplicates = false;
                }
            }

            return fileName;
        }



        private const int baseNum = 62;
        private const int urlLength = 6;
        private const string base62Symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        private string GetUniqueUrl(long number)
        {
            var base62Indexes = GetBase62Indexes(number);

            var result = new StringBuilder(urlLength);
            for (int i = 0; i < urlLength; i++)
                result.Append(base62Symbols[base62Indexes[i]]);

            return result.ToString();
        }

        //private int[] MaskIndexes(int[] base62Indexes)
        //{
        //    var result = new int[urlLength];
        //    for (int i = 0; i < base62Indexes.Length; i++)
        //    {
        //        var indexesSum = base62Indexes[i] + mask[i];
        //        result[i] = indexesSum % baseNum;
        //    }

        //    return result;
        //}

        private int[] GetBase62Indexes(long number)
        {
            var reminders = new List<int>();

            while (true)
            {
                if (number < baseNum)
                {
                    reminders.Add((int)number);
                    break;
                }
                var reminder = (int)number % baseNum;
                reminders.Add(reminder);
                number /= baseNum;
            }
            reminders.Reverse();

            var base62Indexes = new int[urlLength];
            var diff = base62Indexes.Length - reminders.Count;

            for (int i = 0; i < base62Indexes.Length; i++)
                base62Indexes[i] = diff > i ? 0 : reminders[i - diff];

            return base62Indexes;
        }


        public string GenerateUniqueKey()
        {

            var ticks = DateTime.Now.Ticks;
            var guid = Guid.NewGuid().ToString();
            return ticks.ToString() + ' ' + guid;

            //byte[] data = new byte[16];
            //RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            //crypto.GetNonZeroBytes(data);

            //var str =  Convert.ToBase64String(data);

            //return str;
            //long i = 1;

            //foreach (byte b in Guid.NewGuid().ToByteArray())
            //{
            //    i *= ((int)b + 1);
            //}

            //Console.WriteLine(i);
            //Console.WriteLine($"{i:x}");
            //Console.WriteLine(DateTime.Now.Ticks);

            //return $"{(i - DateTime.Now.Ticks):x}";
        }


        static void Main(string[] args)
        {
            var p = new Program();
            Console.WriteLine(p.GenerateUniqueKey());

            //var keys = new List<string>();

            ////get last db record
            //var lastIndex = 0;

            //keys.Add(p.GetUniqueUrl(1563L + lastIndex));


            //Uncommet this
            //var rnd = new Random();
            //keys.Shuffle(rnd);


            //Uncommet this
            //for (int i = lastIndex; i < 10000+lastIndex; i++)
            //{
            //    keys.Add(p.GetUniqueUrl(i));
            //}





            //var uniqueKeys = keys.Select(x => x).Distinct();
            //Console.WriteLine($"Number of unique keys: {uniqueKeys.Count()}");

            //var query = keys.GroupBy(x => x)
            //    .Where(g => g.Count() > 1)
            //    .Select(y => new { value =y.Key, count = y.Count() })
            //    .ToList();

            //if (query.Count>0)
            //{
            //    foreach (var val in query)
            //    {
            //        Console.WriteLine($"{val.value}: {val.count}");
            //    }
            //}
            //else
            //{
            //    Console.WriteLine("No duplicates");
            //}


        }
    }
}

﻿using System.IO;
using System.Threading.Tasks;
using FileStorage.BLL.Models;
using FileStorage.Domain.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FileStorage.PL.Services.Abstract
{
    public interface IHostingFileService
    {
        Task<FileInfoModel> WriteToDisk(IFormFile file, User user);
        void DeleteFromDisk(string name);
        string GetFileType(FileInfo file);
    }
}
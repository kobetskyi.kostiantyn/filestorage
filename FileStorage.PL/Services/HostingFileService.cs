﻿using FileStorage.BLL.Models;
using FileStorage.Domain.Identity;
using FileStorage.PL.Services.Abstract;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FileStorage.PL.Validation;

namespace FileStorage.PL.Services
{
    public class HostingFileService : IHostingFileService
    {
        private readonly IWebHostEnvironment appEnvironment;
        private const string CommonFolder = "Common";
        public HostingFileService(IWebHostEnvironment appEnvironment)
        {
            this.appEnvironment = appEnvironment;
        }

     
        public async Task<FileInfoModel> WriteToDisk(IFormFile file, User user)
        {
            var pathToSave = user == null
                 ? Path.Combine(appEnvironment.WebRootPath, CommonFolder)
                 : Path.Combine(appEnvironment.WebRootPath, $"User-{user.Id}");

            var dir = new DirectoryInfo(pathToSave);
            if (!dir.Exists)
                dir.Create();

            var fileName = file.FileName;
           
            var uniqueServerName = GetUniqueServerName();
            var fullPath = Path.Combine(pathToSave, uniqueServerName);

            await using (var stream = new FileStream(fullPath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            var fileModel = new FileInfoModel()
            {
                Name = fileName,
                Path = fullPath,
                FileSize = file.Length,
                UserId = user?.Id
            };
            
            return fileModel;
        }

        private string GetUniqueServerName()
        {
            var ticks = DateTime.Now.Ticks;
            var guid = Guid.NewGuid().ToString();
            return ticks.ToString() + ' ' + guid;
        }

        public void DeleteFromDisk(string path)
        {
            var file = new FileInfo(path);

            if (!file.Exists)
                throw new PlHostingServiceException("Didn't find file");

            File.Delete(path);
        }

        public string GetFileType(FileInfo file)
        {
            var provider = new FileExtensionContentTypeProvider();
            if (!provider.TryGetContentType(file.Name, out var contentType))
            {
                contentType = "application/octet-stream";
            }

            return contentType;
        }
      
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileStorage.PL.Models.Abstract
{
    public interface IUserCredentials
    {
        string Email { get; set; }
        string Password { get; set; }

    }
}

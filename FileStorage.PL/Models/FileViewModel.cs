﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FileStorage.PL.Models
{
    public class FileViewModel
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(200)]
        public string Name { get; set; }
        public DateTime Changed { get; set; }
        [Required]
        public long FileSize { get; set; }
        [Required]
        public int StatusId { get; set; }
        public string Link { get; set; }
    }
}

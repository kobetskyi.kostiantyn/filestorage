﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FileStorage.BLL.Models;

namespace FileStorage.PL.Models
{
    public class FileResponse
    {
        public FileViewModel File { get; set; }
        public IEnumerable<StatusModel> Statuses { get; set; }
    }
}

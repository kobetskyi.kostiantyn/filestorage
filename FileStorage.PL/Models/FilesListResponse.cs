﻿using System.Collections.Generic;
using FileStorage.BLL.Models;

namespace FileStorage.PL.Models
{
    public class FilesListResponse
    {
        public int FilesCount { get; set; }
        public IEnumerable<FileViewModel> Files { get; set; }
        public IEnumerable<StatusModel> Statuses { get; set; }

    }
}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using FileStorage.Domain.Identity;
using FileStorage.PL.Models;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.Text;
using FileStorage.Domain;
using FileStorage.PL.Models.Abstract;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;

namespace FileStorage.PL.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        
        private readonly IConfiguration configuration;

        public AccountsController(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IConfiguration configuration)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.configuration = configuration;
        }



        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegisterViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = new User() { UserName = model.Email, Email = model.Email };
            var creationResult = await userManager.CreateAsync(user, model.Password);
            if (!creationResult.Succeeded)
                return Unauthorized(creationResult.Errors);

            var roleAssignResult = await userManager.AddToRoleAsync(user, Constants.UserRoleName);
            if (roleAssignResult.Succeeded)
                return Ok(BuildToken(model));
            
            await signInManager.SignOutAsync();
            return Unauthorized(roleAssignResult.Errors);

        }

       
        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await signInManager.PasswordSignInAsync(model.Email, model.Password, isPersistent: false,
#if DEBUG
                lockoutOnFailure: false
#else
                lockoutOnFailure: true
#endif
            );

            if (!result.Succeeded)
                return Unauthorized();


          
            var user =  await userManager.FindByEmailAsync(model.Email);
            var userRole = (await userManager.GetRolesAsync(user)).FirstOrDefault();

            return Ok(BuildToken(model, userRole));
        }

        private TokenInfo BuildToken(IUserCredentials model, string role = Constants.UserRoleName)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JwtKey"]));
            var exp = DateTime.Now.AddDays(1);

            var tokenOptions = new JwtSecurityToken(
                issuer: null,
                audience: null,
                claims: new List<Claim>()
                {
                    new Claim(ClaimTypes.Email, model.Email),
                    new Claim(ClaimTypes.Role, role)
                },
                expires: exp,
                signingCredentials: new SigningCredentials(key, SecurityAlgorithms.HmacSha256)
            );

            var token = new JwtSecurityTokenHandler().WriteToken(tokenOptions);
            return new TokenInfo() { Token = token, Expiration = exp };
        }

    }
}

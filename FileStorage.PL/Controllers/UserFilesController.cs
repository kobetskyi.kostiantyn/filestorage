﻿using FileStorage.BLL.Models;
using FileStorage.BLL.Services.Abstract;
using FileStorage.Domain;
using FileStorage.Domain.Identity;
using FileStorage.PL.Mapping;
using FileStorage.PL.Models;
using FileStorage.PL.Services.Abstract;
using FileStorage.PL.Validation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Security.Claims;
using System.Threading.Tasks;
using FileInfo = System.IO.FileInfo;

namespace FileStorage.PL.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UserFilesController : ControllerBase
    {
        private readonly IFileInfoService filesService;
        private readonly UserManager<User> userManager;
        private readonly IHostingFileService hostingFileService;


        public UserFilesController(
            IFileInfoService filesService,
            UserManager<User> userManager,
            IHostingFileService hostingFileService)
        {
            this.filesService = filesService;
            this.userManager = userManager;
            this.hostingFileService = hostingFileService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] FileInfoFilterModel fileInfoFilter)
        {

            var user = await GetUser();

            var files = await filesService.GetAllUserFiles(user, fileInfoFilter);

            var statuses = filesService.GetStatuses();

            return Ok(files.ToResponseWith(statuses));
        }

        [HttpGet]
        [Route("{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetById(int id)
        {
            var user = await GetUser();

            var file = await filesService.GetByIdAsync(id, user);
            var statuses = filesService.GetStatuses();

            return Ok(file.ToResponseWith(statuses));
        }

        [HttpGet]
        [Route("link/{link}")]
        [AllowAnonymous]
        public IActionResult GetByLink(string link)
        {
            link.Validate();
            var file = filesService.GetByLink(link);
            return Ok(file.ToResponse());
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] FileViewModel model)
        {
            await filesService.UpdateAsync(model.ToFileInfoModel());
            return Ok();
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var user = await GetUser();
            var file = await filesService.GetByIdAsync(id, user);

            hostingFileService.DeleteFromDisk(file.Path);
            await filesService.DeleteByIdAsync(id);
            return Ok();
        }

        [HttpPost]
        [DisableRequestSizeLimit, Route("upload")]
        [AllowAnonymous]
        public async Task<IActionResult> Upload(IFormFile file)
        {
            var user = await GetUser();
            file.Validate(user);

            var fileModel = await hostingFileService.WriteToDisk(file, user);
            fileModel.Id = await filesService.AddFileAsync(fileModel);
            return CreatedAtAction(nameof(GetById), new { id = fileModel.Id }, fileModel);
        }

        [HttpGet]
        [DisableRequestSizeLimit]
        [AllowAnonymous]
        [Route("download/{id}")]
        public async Task<IActionResult> Download(int id)
        {
           
            var user = await GetUser();
            var model = await filesService.GetByIdAsync(id, user);

            var file = new FileInfo(model.Path);
            if (!file.Exists)
                throw new PlHostingServiceException("Didn't find file on server");

            var contentType = hostingFileService.GetFileType(file);

            return PhysicalFile(model.Path, contentType, file.Name);
        }

        private async Task<User> GetUser()
        {
            var userEmail = User.FindFirstValue(ClaimTypes.Email);

            var user = userEmail == null
                ? null
                : await userManager.FindByEmailAsync(userEmail);
            return user;
        }
    }
}

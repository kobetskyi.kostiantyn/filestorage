﻿using FileStorage.BLL.Models;
using FileStorage.BLL.Services.Abstract;
using FileStorage.Domain.Identity;
using FileStorage.PL.Services.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using FileStorage.Domain;

namespace FileStorage.PL.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = "IsAdmin")]
    [Authorize(Roles = Constants.AdministratorRoleName)]
    public class Admin : ControllerBase
    {
        private readonly IFileInfoService filesService;
        private readonly ILogger<UserFilesController> logger;
        private readonly UserManager<User> userManager;
        private readonly IHostingFileService hostingFileService;
        private readonly IWebHostEnvironment appEnvironment;


        public Admin(
            IFileInfoService filesService,
            ILogger<UserFilesController> logger,
            UserManager<User> userManager,
            IHostingFileService hostingFileService,
            IWebHostEnvironment appEnvironment)
        {
            this.filesService = filesService;
            this.logger = logger;
            this.userManager = userManager;
            this.hostingFileService = hostingFileService;
            this.appEnvironment = appEnvironment;
            this.filesService = filesService;
            this.logger = logger;
            this.userManager = userManager;
            this.hostingFileService = hostingFileService;
            this.appEnvironment = appEnvironment;
        }



        [HttpGet]
        public ActionResult<IEnumerable<FileInfoModel>> GetAll()
        {
            var fileInfos = filesService.GetAll();
            return Ok(fileInfos);
        }


        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            if (id < 0)
                return BadRequest("Id can't be less than 1");

            var user = GetUser();

            var fileInfo = await filesService.GetByIdAsync(id);

            if (fileInfo is null)
                return NotFound();

            return Ok(fileInfo);
        }


        private async Task<User> GetUser()
        {
            var userEmail = User.FindFirstValue(ClaimTypes.Email);

            var user = userEmail == null
                ? null
                : await userManager.FindByEmailAsync(userEmail);
            return user;
        }
    }
}

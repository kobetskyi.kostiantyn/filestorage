using AutoMapper;
using FileStorage.BLL.Services;
using FileStorage.DAL;
using FileStorage.DAL.Abstract;
using FileStorage.DAL.Services;
using FileStorage.Domain.Identity;
using FileStorage.PL.Services;
using FileStorage.PL.Services.Abstract;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;
using System.Text;
using FileStorage.BLL.Mapping;
using FileStorage.BLL.Services.Abstract;
using FileStorage.PL.Middleware;

namespace FileStorage.PL
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            #region DB
            services.AddDbContext<FileStorageDbContext>(opt =>
            opt.UseSqlServer(Configuration.GetConnectionString("Default")));
            services.AddTransient<FileStorageDbInitializer>();
            #endregion

            #region Identity

            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<FileStorageDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(opt =>
            {
#if DEBUG
                opt.Password.RequiredLength = 3;
                opt.Password.RequireDigit = false;
                opt.Password.RequireLowercase = false;
                opt.Password.RequiredUniqueChars = 3;
                opt.Password.RequireUppercase = false;
                opt.Password.RequireNonAlphanumeric = false;
                opt.User.RequireUniqueEmail = true;
#endif
            });

            services.AddAuthentication(opt =>
            {
                opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,

                    //ValidIssuer = "https://localhost:5001",
                    //ValidAudience = "https://localhost:5001", 
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtKey"])),
                    ClockSkew = TimeSpan.Zero,

                };
            });

            //services.AddAuthorization(opt =>
            //{
            //    opt.AddPolicy("IsAdmin", policy=>policy.RequireClaim("role", Roles.AdministratorRoleName));
            //});


            #endregion

            #region Cors
            services.AddCors(opt =>
             {
                 opt.AddDefaultPolicy(
                     builder =>
                     {
                         builder.AllowAnyOrigin()
                             .AllowAnyMethod()
                             .AllowAnyHeader();
                     });
             });
            #endregion

            #region Automapper
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutomapperProfile());
            });
            var mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);
            #endregion

            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IFileInfoService, FileInfoService>();
            services.AddTransient<IHostingFileService, HostingFileService>();

            #region Background services
            services.AddHostedService<ScopeForBgService>();
            services.AddScoped<IScopedBgService, ShortLinkPopulateBgService>();
            #endregion

            #region Swagger
            services.AddSwaggerGen(c =>
               {
                   c.SwaggerDoc("v1", new OpenApiInfo() { Title = "Library API", Version = "v1" });
                   var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                   var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                   c.IncludeXmlComments(xmlPath);

                   c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                   {
                       In = ParameterLocation.Header,
                       Description = "Please insert JWT with Bearer into field",
                       Name = "Authorization",
                       Scheme = "Bearer",
                       BearerFormat = "JWT",
                       Type = SecuritySchemeType.ApiKey
                   });

                   c.AddSecurityRequirement(new OpenApiSecurityRequirement
                   {

                       {
                           new OpenApiSecurityScheme
                             {
                                 Reference = new OpenApiReference
                                 {
                                     Type = ReferenceType.SecurityScheme,
                                     Id = "Bearer"
                                 }
                             },
                           new string[] {}
                       }
                   });

               });
            #endregion


            services.Configure<FormOptions>(opt =>
            {
                opt.ValueLengthLimit = int.MaxValue;
                opt.MultipartBodyLengthLimit = int.MaxValue;
                opt.MemoryBufferThreshold = int.MaxValue;
            });

            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, FileStorageDbInitializer dbInitializer)
        {

            dbInitializer.Initialize();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseMiddleware<ExceptionHandlingMiddleware>();

            app.UseCors();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Library Api");
            });

            app.UseHttpsRedirection();

            app.UseRouting();
            
            var provider = new FileExtensionContentTypeProvider();
            provider.Mappings[".myapp"] = "application/x-msdownload";


            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot")),
                ContentTypeProvider = provider
            });

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

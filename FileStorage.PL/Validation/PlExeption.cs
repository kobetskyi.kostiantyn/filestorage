﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileStorage.PL.Validation
{
    public abstract class PlExeption: Exception
    {
        protected PlExeption(string message):base(message)
        {
        }
    }

    public class PlBadRequestException:PlExeption
    {
        public PlBadRequestException(string message) : base(message)
        {
        }
    }
    public class PlHostingServiceException:PlExeption
    {
        public PlHostingServiceException(string message) : base(message)
        {
        }
    }
}

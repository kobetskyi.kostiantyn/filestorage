﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FileStorage.BLL.Validation;
using FileStorage.Domain;
using FileStorage.Domain.Identity;
using Microsoft.AspNetCore.Http;

namespace FileStorage.PL.Validation
{
    public static class FormFileValidation
    {
        public static void Validate(this IFormFile file, User user)
        {
            if (file.Length <= 0)
            {
                throw new PlBadRequestException("File size is equal to 0");
            }

            if (file.Length > Constants.MaxFileSizeForAuthorizedUser)
            {
                throw new PlBadRequestException(string.Format("File is too big. Max size is 1Gb ({0} bytes)", Constants.MaxFileSizeForAuthorizedUser));
            }

            if (string.IsNullOrWhiteSpace(file.FileName))
            {
                throw new PlBadRequestException("File name is null or empty");
            }

            if (file.Name.Length > 200)
            {
                throw new PlBadRequestException("File name is too long");
            }

            if (user is null && file.Length > Constants.MaxFileSizeForUnAuthorizedUsers)
            {
                throw new PlBadRequestException("Not allowed uploading files bigger than 2mb");
            }
        }
    }
}

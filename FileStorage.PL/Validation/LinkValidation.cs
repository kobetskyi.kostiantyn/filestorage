﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FileStorage.Domain;
using FileStorage.PL.Models;

namespace FileStorage.PL.Validation
{
    public static class LinkValidation
    {
        public static void Validate(this string link)
        {
            if (string.IsNullOrWhiteSpace(link) || link.Length < Constants.ShortLinkLength ||
                link.Length > Constants.ShortLinkLength)
                throw new PlBadRequestException("Incorrect link value");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FileStorage.BLL.Models;
using FileStorage.PL.Models;

namespace FileStorage.PL.Mapping
{
    public static class ViewModelMapper
    {
        public static FileViewModel ToViewModel(this FileInfoModel fileInfoModel)
        {
            return new FileViewModel()
            {
                Id = fileInfoModel.Id,
                Name = fileInfoModel.Name,
                FileSize = fileInfoModel.FileSize,
                Link = fileInfoModel.Link,
                StatusId = fileInfoModel.StatusId,
                Changed = fileInfoModel.Changed,

            };
        }

        public static FileInfoModel ToFileInfoModel(this FileViewModel fileViewModel)
        {
            return new FileInfoModel()
            {
                Id = fileViewModel.Id,
                Name = fileViewModel.Name,
                FileSize = fileViewModel.FileSize,
                Link = fileViewModel.Link,
                StatusId = fileViewModel.StatusId,
                Changed = fileViewModel.Changed,
            };
        }

        public static IEnumerable<FileViewModel> ToViewModel(this IEnumerable<FileInfoModel> fileInfoModels)
        {
            return fileInfoModels.Select(x => x.ToViewModel());
        }

        public static FileResponse ToResponseWith(this FileInfoModel fileInfoModel, IEnumerable<StatusModel> statusModels)
        {
            return new FileResponse()
            {
                File = fileInfoModel.ToViewModel(),
                Statuses = statusModels,
            };
        }

        public static FilesListResponse ToResponseWith(this PagedFileModelList pagedFileModelList,
            IEnumerable<StatusModel> statusModels)
        {
            return new FilesListResponse()
            {
                Files = pagedFileModelList.ToViewModel(),
                Statuses = statusModels,
                FilesCount = pagedFileModelList.TotalCount,
            };
        }


        public static FileViewModel ToResponse(this FileInfoModel fileInfoModel)
        {
            return fileInfoModel.ToViewModel();
        }
    }
}

﻿using System.Linq;
using AutoMapper;
using FileStorage.BLL.Models;
using FileStorage.Domain;
using FileStorage.Domain.Identity;

namespace FileStorage.BLL.Mapping
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {

            #region FileInfo

            CreateMap<FileInfo, FileInfoModel>()
                .ForMember(f => f.Link, opt => opt.MapFrom(src => src.ShortLink.Value))
                .ForMember(f => f.StatusId, opt => opt.MapFrom(src => src.FileStatus.Id));
                

            CreateMap<FileInfoModel, FileInfo>()
                .ForMember(f => f.FileStatus, opt => opt.Ignore())
                .ForMember(f => f.ShortLink, opt => opt.Ignore());
                
            
            #endregion
            
            #region User
            CreateMap<User, UserModel>()
                  .ForMember(f => f.Email, opt => opt.MapFrom(src => src.Email))
                  .ForMember(f => f.Id, opt => opt.MapFrom(src => src.Id))
                  .ForMember(f => f.FileInfoIds, opt => opt.MapFrom(src => src.FileInfos.Select(x => x.Id).ToList()))
                  .ReverseMap();
            #endregion

            #region ShortLink
            CreateMap<ShortLink, ShortLinkModel>()
                    .ForMember(s => s.Id, opt => opt.MapFrom(src => src.Id))
                    .ForMember(s => s.Value, opt => opt.MapFrom(src => src.Value))
                    .ReverseMap();
            #endregion

            #region FileStatus
            CreateMap<FileStatus, StatusModel>()
                 .ForMember(s => s.Value, opt => opt.MapFrom(src => src.StatusName))
                 .ReverseMap();
            #endregion
        }
    }
}

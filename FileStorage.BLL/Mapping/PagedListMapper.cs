﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FileStorage.BLL.Models;
using FileStorage.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FileStorage.BLL.Mapping
{
    public static class PagedListMapper
    {
        public async static Task<PagedFileModelList> ToPagedFileList(this IQueryable<FileInfo> source, int pageNumber, int pageSize, IMapper mapper)
        {
            var count = await source.CountAsync();
            var items = source.Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ToList();
            
            var fileModels = mapper.Map<IEnumerable<FileInfoModel>>(items).ToList();

            return new PagedFileModelList(fileModels, count, pageNumber, pageSize);
        }
    }
}

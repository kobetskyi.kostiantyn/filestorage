﻿using System.Collections.Generic;
using FileStorage.Domain.Identity;

namespace FileStorage.BLL.Models
{
    public class UserModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public IEnumerable<int> FileInfoIds { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FileStorage.BLL.Models
{
    public class FileInfoModel
    {
        public int  Id { get; set; }
        public string Name { get; set; }
        public int? UserId { get; set; }
        public DateTime Changed { get; set; }
        public long FileSize { get; set; }
        public int StatusId { get; set; }
        public string Link { get; set; }
        public string Path { get; set; }

    }
}

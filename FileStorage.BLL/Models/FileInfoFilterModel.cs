﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileStorage.BLL.Models
{
    public class FileInfoFilterModel
    {
        private const int maxPageSize = 50;
        private int pageSize = 5;
        public int PageNumber { get; set; } = 1;
        public int PageSize
        {
            get => pageSize;
            set => pageSize = (value > maxPageSize) ? maxPageSize : value;
        }

    }
}

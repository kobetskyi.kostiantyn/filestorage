﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileStorage.BLL.Models
{
    public class ShortLinkModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileStorage.Domain;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace FileStorage.BLL.Models
{
    public class PagedFileModelList : List<FileInfoModel>
    {
        public int CurrentPage { get; private set; }
        //public int TotalPages { get; private set; }
        public int PageSize { get; private set; }
        public int TotalCount { get; private set; }

        public PagedFileModelList(List<FileInfoModel> items, int count, int pageNumber,
            int pageSize)
        {
            TotalCount = count;
            PageSize = pageSize;
            CurrentPage = pageNumber;
            //TotalPages = (int)Math.Ceiling(count / (double)pageSize);

            
            AddRange(items);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileStorage.BLL.Validation
{
    public abstract class BllException : Exception
    {
        protected BllException(string message) : base(message)
        {

        }
    }

    public class BllNotFoundException: BllException
    {
        public BllNotFoundException(string message) : base(message)
        {
        }
    }
    public class BllBadRequestException: BllException
    {
        public BllBadRequestException(string message) : base(message)
        {
        }
    }
}

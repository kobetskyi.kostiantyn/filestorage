﻿using System;
using System.Collections.Generic;
using System.Text;
using FileStorage.BLL.Models;

namespace FileStorage.BLL.Validation
{
    public static class FileInfoModelValidation
    {
        private static void BaseValidate(FileInfoModel fileInfoModel)
        {
            if (string.IsNullOrWhiteSpace(fileInfoModel.Name))
            {
                throw new BllBadRequestException("File name is empty");
            }

            if (fileInfoModel.Name.Length > 200)
            {
                throw new BllBadRequestException("File name length is more than 200 characters");
            }

            if (fileInfoModel.FileSize <= 0)
            {
                throw new BllBadRequestException("File size is equal or less than 0");
            }
        }
        public static void ValidatePutModel(this FileInfoModel fileInfoModel)
        {
            BaseValidate(fileInfoModel);
            if (fileInfoModel.Id <= 0)
            {
                throw new BllBadRequestException("File id is equal or less than 0");
            }
            if (fileInfoModel.StatusId < 1 || fileInfoModel.StatusId > 2)
            {
                throw new BllBadRequestException("Incorrect file status value");
            }

            if (string.IsNullOrWhiteSpace(fileInfoModel.Link))
            {
                throw new BllBadRequestException("File link field is empty");
            }

        }
        
        public static void ValidatePostModel(this FileInfoModel fileInfoModel)
        {
            BaseValidate(fileInfoModel);
            if (string.IsNullOrWhiteSpace(fileInfoModel.Path))
            {
                throw new BllBadRequestException("Path field is null or empty");
            }
        }
    }
}

﻿using AutoMapper;
using FileStorage.BLL.Models;
using FileStorage.DAL.Abstract;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FileStorage.BLL.Mapping;
using FileStorage.BLL.Services.Abstract;
using FileStorage.BLL.Validation;
using FileStorage.Domain;
using FileStorage.Domain.Identity;
using Microsoft.AspNetCore.Identity;
using FileInfo = FileStorage.Domain.FileInfo;

namespace FileStorage.BLL.Services
{
    public class FileInfoService : IFileInfoService
    {
        private readonly IUnitOfWork uow;
        private readonly IMapper mapper;
        private readonly ILogger<FileInfoService> logger;

        public FileInfoService(
            IUnitOfWork uow,
            IMapper mapper,
            ILogger<FileInfoService> logger)
        {
            this.uow = uow;
            this.mapper = mapper;
            this.logger = logger;
        }

        public IEnumerable<FileInfoModel> GetAll()
        {
            var fileInfos = uow.FileInfoRepository.GetAllWithDetails().AsEnumerable();
            var models = mapper.Map<IEnumerable<FileInfoModel>>(fileInfos).ToArray();
            return models;
        }

        public FileInfoModel GetByLink(string link)
        {
            var fileInfo = uow.FileInfoRepository.GetAllWithDetails().Where(f => f.FileStatusId == 2)
                  .FirstOrDefault(f => f.ShortLink.Value == link);

            if (fileInfo is null)
                throw new BllNotFoundException(String.Format("There is no file with such link:{0}",link));

            var model = mapper.Map<FileInfoModel>(fileInfo);
            return model;
        }

        public IEnumerable<StatusModel> GetStatuses()
        {
            var statuses = uow.FileStatusRepository.FindAll();
            return mapper.Map<IEnumerable<StatusModel>>(statuses);
        }

        public async Task<FileInfoModel> GetByIdAsync(int id, User user = null)
        {
            if (id <= 0)
                throw new BllBadRequestException("Id is equal or less than 0");

            var fileInfo = new FileInfo();
            if (user is null)
            {
                fileInfo = uow.FileInfoRepository
                    .GetAllWithDetails()
                    .Where(f => f.User == null)
                    .FirstOrDefault(f => f.Id == id);
            }
            else
            {
                fileInfo = uow.FileInfoRepository
                    .GetAllWithDetails()
                    .Where(f => f.UserId == user.Id)
                    .FirstOrDefault(f => f.Id == id);
            }

            if (fileInfo is null)
                throw new BllNotFoundException(String.Format("There is no file with such id {0}", id));

            var model = mapper.Map<FileInfoModel>(fileInfo);

            return (model);
        }

        public async Task<PagedFileModelList> GetAllUserFiles(User user, FileInfoFilterModel filter = null)
        {
            filter ??= new FileInfoFilterModel();

            if (user is null)
                throw new BllBadRequestException("User is null");

            var fileInfos = uow.FileInfoRepository.GetAllWithDetails()
                .Where(f => f.UserId == user.Id)
                .OrderByDescending(f => f.Created);

            var pagedFileList = await fileInfos.ToPagedFileList(filter.PageNumber, filter.PageSize, mapper);

            return pagedFileList;
        }

        public async Task<int> AddFileAsync(FileInfoModel model)
        {
            model.ValidatePostModel();

            var fileInfo = mapper.Map<FileInfo>(model);
            if (model.UserId is null)
                fileInfo.User = null;

            fileInfo.FileStatusId = 2;

            var counter = 0;
            bool dbSaveSuccess;
            do
            {
                var link = await uow.ShortLinkRepository
                    .FindAll()
                    .FirstOrDefaultAsync(l => l.Taken == false);

                fileInfo.ShortLink = link;
                link.Taken = true;
                await uow.FileInfoRepository.AddAsync(fileInfo);
                uow.ShortLinkRepository.Update(link);
                try
                {
                    dbSaveSuccess = true;
                    await uow.SaveAsync();
                }
                catch (DbUpdateConcurrencyException e)
                {
                    counter++;
                    dbSaveSuccess = false;
                    logger.LogError(e, "DbUpdateConcurrencyException. Attempt № {2} FileInfo Id: {0} Shortlink Id: {1}",
                        fileInfo.Id, link.Id, counter);
                    await e.Entries.Single().ReloadAsync();
                }

            } while (!dbSaveSuccess);

            return fileInfo.Id;
        }

        public async Task<int> UpdateAsync(FileInfoModel model)
        {
            model.ValidatePutModel();

            var fileInfo = await uow.FileInfoRepository.GetByIdWithDetailsAsync(model.Id);
            model.Path = fileInfo.Path;
            model.UserId = fileInfo.UserId;

            fileInfo = mapper.Map(model, fileInfo);

            fileInfo.FileStatusId = model.StatusId;
            fileInfo.Changed = DateTime.Now;

            uow.FileInfoRepository.Update(fileInfo);
            return await uow.SaveAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            await uow.FileInfoRepository.DeleteByIdAsync(id);
            await uow.SaveAsync();
        }

    }
}

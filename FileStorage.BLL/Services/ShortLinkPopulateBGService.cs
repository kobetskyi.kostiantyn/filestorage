﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FileStorage.BLL.Models;
using FileStorage.BLL.Services.Abstract;
using FileStorage.DAL;
using FileStorage.Domain;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace FileStorage.BLL.Services
{
    public class ShortLinkPopulateBgService : IScopedBgService
    {
        private readonly ILogger<ShortLinkPopulateBgService> logger;
        private readonly FileStorageDbContext dbContext;
        private readonly IConfiguration configuration;


        private const int baseNum = 62;
        private const int urlLength = 7;
        private const string base62Symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        public ShortLinkPopulateBgService(
            ILogger<ShortLinkPopulateBgService> logger,
            FileStorageDbContext dbContext,
            IConfiguration configuration)
        {
            this.logger = logger;
            this.dbContext = dbContext;
            this.configuration = configuration;
            this.configuration = configuration;
        }

        public async Task DoWorkAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                logger.LogInformation("ShortLinkPopulateService task doing background work");

                if (CheckIfPopulationNeeded())
                {
                    logger.LogInformation("Population required. Starting population method");
                    Populate();
                }
                else
                {
                    logger.LogInformation("Population not needed");
                }

                var settings = configuration.GetSection("ShortLinkPopulationSettings");
                var checkTimeInterval = int.Parse(settings.GetSection("CheckTimeInterval").Value);


                await Task.Delay(checkTimeInterval, stoppingToken);
            }
        }


        private bool CheckIfPopulationNeeded()
        {
            if (!dbContext.Database.CanConnect())
                return false;

            var actualFreeLinksCount = 0;
            var totalLinks = 0;
            try
            {
                totalLinks = dbContext.ShortLinks.Count();
                actualFreeLinksCount = dbContext.ShortLinks.Count(l => l.Taken == false);
            }
            catch (Exception e)
            {
                logger.LogError(e, "Table 'ShortLinks' doesn't exist. Population impossible");
                return false;
            }

            var criticalFreelinksCount = int.Parse(configuration.GetSection("ShortLinkPopulationSettings").GetSection("LinksLeftBeforePopulation").Value);
            //todo check correct values
            
            logger.LogInformation("Actual free links left: {0} Population starts on {1} free links left. Total links: {2}", actualFreeLinksCount, criticalFreelinksCount, totalLinks);
            return actualFreeLinksCount <= criticalFreelinksCount;
        }
        private string GetUniqueUrl(long number)
        {
            var base62Indexes = GetBase62Indexes(number);

            var result = new StringBuilder(urlLength);
            for (int i = 0; i < urlLength; i++)
                result.Append(base62Symbols[base62Indexes[i]]);

            return result.ToString();
        }
        private int[] GetBase62Indexes(long number)
        {
            var reminders = new List<int>();

            while (true)
            {
                if (number < baseNum)
                {
                    reminders.Add((int)number);
                    break;
                }
                var reminder = (int)number % baseNum;
                reminders.Add(reminder);
                number /= baseNum;
            }
            reminders.Reverse();

            var base62Indexes = new int[urlLength];
            var diff = base62Indexes.Length - reminders.Count;

            for (int i = 0; i < base62Indexes.Length; i++)
                base62Indexes[i] = diff > i ? 0 : reminders[i - diff];

            return base62Indexes;
        }
        private void Shuffle(List<ShortLink> list)
        {
            var rnd = new Random();

            var n = list.Count;
            while (n > 1)
            {
                var k = rnd.Next(n--);
                (list[n], list[k]) = (list[k], list[n]);
            }
        }

        public void Populate()
        {
            var lastIndex = dbContext.ShortLinks.Count();
            var count = int.Parse(configuration.GetSection("ShortLinkPopulationSettings").GetSection("PopulateAtOnce").Value) + lastIndex;



            var linkCollection = new List<ShortLink>();
            for (var i = lastIndex + 1; i <= count; i++)
            {
                linkCollection.Add(new ShortLink() { Value = GetUniqueUrl(i) });
            }

            Shuffle(linkCollection);

            using (dbContext.Database.BeginTransaction())
            {
                dbContext.AddRange(linkCollection);
                dbContext.SaveChanges();
                dbContext.Database.CommitTransaction();
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FileStorage.BLL.Models;
using FileStorage.Domain.Identity;

namespace FileStorage.BLL.Services.Abstract
{
    public interface IFileInfoService
    {
        Task<int> AddFileAsync(FileInfoModel model);
        Task DeleteByIdAsync(int id);
        IEnumerable<FileInfoModel> GetAll();
        Task<PagedFileModelList> GetAllUserFiles(User user, FileInfoFilterModel filter = null);
        Task<FileInfoModel> GetByIdAsync(int id, User user = null);
        Task<int> UpdateAsync(FileInfoModel model);
        FileInfoModel GetByLink(string link);
        IEnumerable<StatusModel> GetStatuses();
    }
}
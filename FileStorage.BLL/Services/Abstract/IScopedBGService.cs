﻿using System.Threading;
using System.Threading.Tasks;

namespace FileStorage.BLL.Services.Abstract
{
    public interface IScopedBgService
    {
        Task DoWorkAsync(CancellationToken stoppingToken);
    }
}

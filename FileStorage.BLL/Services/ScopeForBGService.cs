﻿using FileStorage.BLL.Services.Abstract;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FileStorage.BLL.Services
{
    public class ScopeForBgService : BackgroundService
    {
        private readonly ILogger<ScopeForBgService> logger;

        private readonly IServiceProvider serviceProvider;

        public ScopeForBgService(
            ILogger<ScopeForBgService> logger,
            IServiceProvider serviceProvider)
        {
            this.logger = logger;
            this.serviceProvider = serviceProvider;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            logger.LogInformation("ShortLinkPopulateService is starting");

            await DoWorkAsync(stoppingToken);

        }

        private async Task DoWorkAsync(CancellationToken stoppingToken)
        {
            logger.LogInformation("ShortLinkPopulateService is working");
            using (var scope = serviceProvider.CreateScope())
            {
                var scopedService =
                    scope.ServiceProvider.GetRequiredService<IScopedBgService>();

                await scopedService.DoWorkAsync(stoppingToken);
            }

        }

        public override async Task StopAsync(CancellationToken stoppingToken)
        {
            logger.LogInformation(
                $"ShortLinkPopulateService is stopping.");

            await base.StopAsync(stoppingToken);
        }
    }
}

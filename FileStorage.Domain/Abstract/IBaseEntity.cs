﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileStorage.Domain.Abstract
{
    public interface IBaseEntity
    {
        int Id { get; set; }
    }
}

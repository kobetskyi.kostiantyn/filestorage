﻿using System;
using System.Collections.Generic;
using System.Text;
using FileStorage.Domain.Abstract;
using Microsoft.AspNetCore.Identity;

namespace FileStorage.Domain.Identity
{
    public class User : IdentityUser<int>, IBaseEntity
    {
        public ICollection<FileInfo> FileInfos { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileStorage.Domain
{
    public static class Constants
    {
        public const string AdministratorRoleName = "Administrator";

        public const string UserRoleName = "User";

        public const int MaxFileSizeForAuthorizedUser = 1073741824;
        public const int MaxFileSizeForUnAuthorizedUsers = 2097152;
        public const int ShortLinkLength = 7;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using FileStorage.Domain.Abstract;

namespace FileStorage.Domain
{
    public class FileStatus:BaseEntity
    {
        public string StatusName { get; set; }
        public ICollection<FileInfo> FileInfos { get; set; }
    }
}

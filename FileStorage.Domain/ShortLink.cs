﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using FileStorage.Domain.Abstract;

namespace FileStorage.Domain
{
    public class ShortLink : BaseEntity
    {
        public string Value { get; set; }
        public bool Taken { get; set; }
        public FileInfo FileInfo { get; set; }
        public byte[] ConcurrencyToken { get; set; }
    }
}

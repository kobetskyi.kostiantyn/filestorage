﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FileStorage.Domain.Abstract;
using FileStorage.Domain.Identity;
using Microsoft.AspNetCore.Identity;

namespace FileStorage.Domain
{
    public class FileInfo : BaseEntity
    {
        public string Name { get; set; }
        public long FileSize { get; set; }
        public string Path { get; set; }
        public DateTime Created { get; set; }
        public DateTime Changed { get; set; }
        public int ShortLinkId { get; set; }
        public ShortLink ShortLink { get; set; }
        public int  FileStatusId { get; set; }
        public FileStatus FileStatus { get; set; }
        public int? UserId { get; set; }
        public User User { get; set; }
    }
}
